# Configuration Framework

An interface-based framework for configuring classes and saving and loading from file.

# How To Use

There is an example of how to use this framework in the `Example Use Case` folder.

# Contribution guidelines

- All code should be written in **LabVIEW 2020**
- Pull requests should be small and concise.

# Questions and Comments

Email [support@mooregoodideas.com](mailto:support@mooregoodideas.com) for comments.
Also consider using this repository's [Issue Tracker](https://gitlab.com/mgi/configuration-framework/issues) to submit a bug report or feature request.
