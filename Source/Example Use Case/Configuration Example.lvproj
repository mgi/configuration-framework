﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="gpm_packages" Type="Folder">
			<Item Name="@mgi" Type="Folder">
				<Item Name="json" Type="Folder">
					<Item Name="JSON" Type="Folder">
						<Item Name="JSON Data" Type="Folder">
							<Item Name="Array" Type="Folder"/>
							<Item Name="Boolean" Type="Folder"/>
							<Item Name="Null" Type="Folder"/>
							<Item Name="Number" Type="Folder"/>
							<Item Name="Object" Type="Folder"/>
							<Item Name="Raw JSON" Type="Folder"/>
							<Item Name="String" Type="Folder"/>
						</Item>
						<Item Name="JSON.lvlib" Type="Library" URL="../../gpm_packages/@mgi/json/JSON/JSON.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../../gpm_packages/@mgi/json/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../../gpm_packages/@mgi/json/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../../gpm_packages/@mgi/json/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../../gpm_packages/@mgi/json/README.md"/>
				</Item>
				<Item Name="library-core" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="MGI-Library Core.lvlib" Type="Library" URL="../../gpm_packages/@mgi/library-core/Source/MGI-Library Core.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../../gpm_packages/@mgi/library-core/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../../gpm_packages/@mgi/library-core/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../../gpm_packages/@mgi/library-core/LICENSE"/>
					<Item Name="readme.md" Type="Document" URL="../../gpm_packages/@mgi/library-core/readme.md"/>
				</Item>
				<Item Name="monitored-actor" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="Classic Monitor" Type="Folder"/>
						<Item Name="Classic Monitor Messages" Type="Folder">
							<Item Name="Log Messages Msg" Type="Folder"/>
						</Item>
						<Item Name="Logger" Type="Folder">
							<Item Name="Logger Actor" Type="Folder"/>
							<Item Name="Logger Messages" Type="Folder">
								<Item Name="Clear Log Msg" Type="Folder"/>
								<Item Name="New Message Msg" Type="Folder"/>
								<Item Name="Start Logging Msg" Type="Folder"/>
								<Item Name="Stop Logging Msg" Type="Folder"/>
								<Item Name="Update Monitor Label Msg" Type="Folder"/>
							</Item>
						</Item>
						<Item Name="Orphan Detector" Type="Folder">
							<Item Name="Orphan Detector" Type="Folder"/>
							<Item Name="Orphan Detector Messages" Type="Folder">
								<Item Name="Monitor Stop Msg" Type="Folder"/>
							</Item>
						</Item>
						<Item Name="MGI-Monitored Actor.lvlib" Type="Library" URL="../../gpm_packages/@mgi/monitored-actor/Source/MGI-Monitored Actor.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../../gpm_packages/@mgi/monitored-actor/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../../gpm_packages/@mgi/monitored-actor/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../../gpm_packages/@mgi/monitored-actor/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../../gpm_packages/@mgi/monitored-actor/README.md"/>
				</Item>
				<Item Name="monitored-actor-core" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="Actor Monitor" Type="Folder">
							<Item Name="Actor Monitor" Type="Folder"/>
							<Item Name="Actor Monitor Messages" Type="Folder">
								<Item Name="Actor Launched Msg" Type="Folder"/>
								<Item Name="Actor Stopped Msg" Type="Folder"/>
								<Item Name="Log Message Msg" Type="Folder"/>
								<Item Name="Manually Remove Actor Msg" Type="Folder"/>
								<Item Name="Ping Actor Msg" Type="Folder"/>
								<Item Name="Report Ping Msg" Type="Folder"/>
								<Item Name="Update Label Msg" Type="Folder"/>
							</Item>
						</Item>
						<Item Name="Monitor Data" Type="Folder"/>
						<Item Name="Monitored Actor" Type="Folder">
							<Item Name="Monitored Actor" Type="Folder"/>
							<Item Name="Monitored Actor Messages" Type="Folder">
								<Item Name="Disable Message Logging Msg" Type="Folder"/>
								<Item Name="Enable Message Logging Msg" Type="Folder"/>
								<Item Name="Notify Monitor Msg" Type="Folder"/>
								<Item Name="Ping Msg" Type="Folder"/>
							</Item>
						</Item>
						<Item Name="MGI-Monitored Actor Core.lvlib" Type="Library" URL="../../gpm_packages/@mgi/monitored-actor-core/Source/MGI-Monitored Actor Core.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../../gpm_packages/@mgi/monitored-actor-core/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../../gpm_packages/@mgi/monitored-actor-core/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../../gpm_packages/@mgi/monitored-actor-core/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../../gpm_packages/@mgi/monitored-actor-core/README.md"/>
				</Item>
				<Item Name="panel-actors" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="Panel Actor" Type="Folder"/>
						<Item Name="Panel Actor Messages" Type="Folder">
							<Item Name="Change Panel Msg" Type="Folder"/>
							<Item Name="Hide Panel Msg" Type="Folder"/>
							<Item Name="Launch Nested Panel Message Msg" Type="Folder"/>
							<Item Name="Show Panel Msg" Type="Folder"/>
						</Item>
						<Item Name="MGI-Panel Actor.lvlib" Type="Library" URL="../../gpm_packages/@mgi/panel-actors/Source/MGI-Panel Actor.lvlib"/>
						<Item Name="Panel Actor Event Loop.vi" Type="VI" URL="../../gpm_packages/@mgi/panel-actors/Source/Panel Actor Event Loop.vi"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../../gpm_packages/@mgi/panel-actors/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../../gpm_packages/@mgi/panel-actors/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../../gpm_packages/@mgi/panel-actors/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../../gpm_packages/@mgi/panel-actors/README.md"/>
				</Item>
				<Item Name="panel-manager" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="Cursor" Type="Folder"/>
						<Item Name="Deferer" Type="Folder"/>
						<Item Name="Init Waiter" Type="Folder"/>
						<Item Name="Panel" Type="Folder">
							<Item Name="Subpanel" Type="Folder"/>
							<Item Name="Window" Type="Folder">
								<Item Name="Dialog" Type="Folder"/>
							</Item>
						</Item>
						<Item Name="Panel Control Counter" Type="Folder"/>
						<Item Name="Panel Helper" Type="Folder"/>
						<Item Name="Panel Registry" Type="Folder"/>
						<Item Name="Panel Updater" Type="Folder">
							<Item Name="Window Updater" Type="Folder"/>
						</Item>
						<Item Name="Window Position" Type="Folder">
							<Item Name="Maximize" Type="Folder"/>
							<Item Name="Persistent Position" Type="Folder"/>
							<Item Name="Reference Position" Type="Folder">
								<Item Name="Offset" Type="Folder">
									<Item Name="Absolute Offset" Type="Folder"/>
									<Item Name="Cascade" Type="Folder"/>
								</Item>
								<Item Name="Ref Ctrl" Type="Folder"/>
								<Item Name="Ref Display" Type="Folder"/>
								<Item Name="Ref Rectangle" Type="Folder"/>
								<Item Name="Ref Top Level" Type="Folder"/>
								<Item Name="Ref VI" Type="Folder"/>
								<Item Name="Size" Type="Folder">
									<Item Name="Absolute Size" Type="Folder"/>
									<Item Name="Auto-Stretch" Type="Folder"/>
									<Item Name="Reference Size" Type="Folder"/>
								</Item>
							</Item>
						</Item>
						<Item Name="MGI-Panel Manager.lvlib" Type="Library" URL="../../gpm_packages/@mgi/panel-manager/Source/MGI-Panel Manager.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../../gpm_packages/@mgi/panel-manager/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../../gpm_packages/@mgi/panel-manager/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../../gpm_packages/@mgi/panel-manager/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../../gpm_packages/@mgi/panel-manager/README.md"/>
				</Item>
			</Item>
		</Item>
		<Item Name="Example Use Case" Type="Folder">
			<Item Name="Thermometer Types" Type="Folder">
				<Item Name="Abstract Thermometer" Type="Folder">
					<Item Name="Thermometer Config.lvclass" Type="LVClass" URL="../Thermometer Config/Thermometer Config.lvclass"/>
					<Item Name="Thermometer Setup Screen.lvlib" Type="Library" URL="../Thermometer Setup Screen/Thermometer Setup Screen.lvlib"/>
					<Item Name="Thermometer.lvclass" Type="LVClass" URL="../Thermometer/Thermometer.lvclass"/>
				</Item>
				<Item Name="I2C Thermometer" Type="Folder">
					<Item Name="I2C Thermometer Config.lvclass" Type="LVClass" URL="../I2C Thermometer Config/I2C Thermometer Config.lvclass"/>
					<Item Name="I2C Thermometer Setup Screen.lvlib" Type="Library" URL="../I2C Thermometer Setup Screen/I2C Thermometer Setup Screen.lvlib"/>
					<Item Name="I2C Thermometer.lvclass" Type="LVClass" URL="../I2C Thermometer/I2C Thermometer.lvclass"/>
				</Item>
				<Item Name="Modbus Thermometer" Type="Folder">
					<Item Name="Modbus Thermometer Config.lvclass" Type="LVClass" URL="../Modbus Thermometer Config/Modbus Thermometer Config.lvclass"/>
					<Item Name="Modbus Thermometer Setup Screen.lvlib" Type="Library" URL="../Modbus Thermometer Setup Screen/Modbus Thermometer Setup Screen.lvlib"/>
					<Item Name="Modbus Thermometer.lvclass" Type="LVClass" URL="../Modbus Thermometer/Modbus Thermometer.lvclass"/>
				</Item>
			</Item>
			<Item Name="Main.lvlib" Type="Library" URL="../Main/Main.lvlib"/>
			<Item Name="Splash Screen.vi" Type="VI" URL="../Splash Screen.vi"/>
		</Item>
		<Item Name="Setup Screen.lvlib" Type="Library" URL="../../Setup Screen/Setup Screen.lvlib"/>
		<Item Name="Config Framework.lvlib" Type="Library" URL="../../Config Framework/Config Framework.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Single String To Qualified Name Array.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Single String To Qualified Name Array.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="OffsetRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/OffsetRect.vi"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="LVRowAndColumnUnsignedTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnUnsignedTypeDef.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="RGB to Color.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/RGB to Color.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="Casting Utility For Actors.vim" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Actor/Casting Utility For Actors.vim"/>
				<Item Name="Panel.lvlib" Type="Library" URL="/&lt;vilib&gt;/MGI/Panel Manager/Panel/Panel.lvlib"/>
				<Item Name="Random Number (Range).vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range).vi"/>
				<Item Name="Random Number (Range) DBL.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) DBL.vi"/>
				<Item Name="Random Number (Range) I64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) I64.vi"/>
				<Item Name="sub_Random U32.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/sub_Random U32.vi"/>
				<Item Name="Random Number (Range) U64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) U64.vi"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
